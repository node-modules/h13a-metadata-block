const Promise = require("bluebird");

const util = require("h13a-util");

const readme = require("../../../../README.md");
const packageJson = require("../../../../package.json");

class Operation {

  constructor({
    cardTask
  }) {
    this.cardTask = cardTask;
  }

  connection({
      resolve,
      reject,
      element,
      block
    }) {

      let {
        type // string, what type of metadata is requested
      } = block;

      let types = type.split(",").map(i => i.trim()).filter(i => i.length);

      // for every type of metadata requested
      types.forEach(type => {

        // if that request can be fufilled
        if (this[type + 'Metadata']) {
          // execute the method
          let $content = this[type + 'Metadata']().addClass('card-component');
          element.append($content);
        }
      });
      resolve();

    } // end connection

  readmeMetadata() {
    let $content = $(`<div class="card-block card-text readme-position" style="min-height: 6rem;"></div>`);
    $content.append(readme);
    return $content;
  }

  optionsMetadata() {
    let $content = $(`<div class="card-block card-text options-position hidden-xs" style="min-height: 6rem;"></div>`);
    let high = JSON.stringify(this.cardTask.widget.option('_raw'), null, ' ');
    high = high.replace(/</g, '&lt;').replace(/>/g, '&gt;');
    $content.append(`<pre><code class="lime">` + high + `</code></pre>`);
    return $content;
  }

  dependenciesMetadata() {

    let $content = $(`<div class="card-block card-text dependency-position" style="min-height: 6rem;"></div>`);


    let makeNpmRow = (item, index) => {

      return `
        <tr class="">
         <td class="text-info p-a-1">
          <div class="outie-sm p-a-1 bg-blue stripes-lg">
            <a href="https://www.npmjs.com/package/${item.key}" class="black" style="text-decoration: none;" target="_blank">${item.key}</a>
          </div>
         </td>
         <td class="text-info p-a-1"><div class="outie-sm p-a-1 bg-olive black stripes-md">${item.value.substr(0,9)}</div></td>
        </tr>
        `;
    };
    let makeGithubRow = (item, index) => {

      //git+https://github.com/mike-hearn/transparent-textures.git

      let matches = item.value.match(/\:\/\/github\.com\/((.+)\/(.+))\.git/);
      let name = matches[3];
      let url = 'https://github.com/' + matches[1];

      return `
        <tr class="">
        <td class="text-info p-a-1">
           <div class="outie-sm p-a-1 bg-blue black stripes-lg">
             <a href="${url}" class="black" style="text-decoration: none;" target="_blank">${name}</a>
           </div>
         </td>
         <td class="text-info p-a-1"><div class="outie-sm p-a-1 bg-olive black stripes-md text-center"><i class="fa fa-github-alt"></i></div></td>
        </tr>
        `;
    };
    let makeGitlabRow = (item, index) => {

      let matches = item.value.match(/\:\/\/gitlab\.com\/((.+)\/(.+))\.git/);
      let name = matches[3];
      let url = 'https://gitlab.com/' + matches[1];

      return `
        <tr class="">
        <td class="text-info p-a-1">
           <div class="outie-sm p-a-1 bg-blue black stripes-lg">
             <a href="${url}" class="black" style="text-decoration: none;" target="_blank">${name}</a>
           </div>
         </td>
         <td class="text-info p-a-1"><div class="outie-sm p-a-1 bg-olive navy stripes-md text-center"><i class="fa fa-git"></i></div></td>
        </tr>
        `;
    };


    let makeRow = (item, index) => {

      if(item.value.match(/gitlab.com/)){
        return makeGitlabRow(item, index);
      }else if(item.value.match(/github.com/)){
        return makeGithubRow(item, index);
      }else{
        return makeNpmRow(item, index);
      }

    };

    let makeTable = (el) => {
      el.append(`
        <table class="table table-sm table-hover" style="background: none;">
          <thead class="thead-inverse">
            <tr>
              <th class="text-info">dependency</th>
              <th class="text-info">version</th>
            </tr>
          </thead>
          <tbody>
             ${util.ota( packageJson.devDependencies ).map(makeRow).join('')}
          </tbody>
        </table>
        `);
    };
    makeTable($content);
    return $content;
  }

} // close Operation

module.exports = Operation;
