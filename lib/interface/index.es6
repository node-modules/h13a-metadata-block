const Promise = require("bluebird");
const Operation = require("../operation/index.es6");

class Interface {

  constructor({ task, block }) {

    this.cardTask = task;
    this.blockMetadata = block;

  }

  creator() {

    let cardBlockPromise = new Promise((resolve, reject) => {

      let { element } = this.cardTask.widget;
      let { cardTask, blockMetadata } = this;
      let operation = new Operation({ cardTask });

      operation.connection({

        // Promise
        resolve,
        reject,

        // Helpful Information Isolated for the specific task at hand
        element, // print to this
        block: blockMetadata, // use information from here

      });

    }); // end creation of cardBlockPromise
    return cardBlockPromise;
  }

}

module.exports = Interface;
